#!/bin/sh

CVSROOT=/cvs/gnome
SVNROOT=/svn
OUTDIR=/svn/tmp

TMPDIR=/dev/shm
#TMPDIR=/var/tmp

function migrate() {
	# Ignore symlinks
	if [ -h $CVSROOT/$i ]; then
		echo "Skipping symlink '$i'."
		continue
	fi

	# Already done?
	if [ -d $SVNROOT/$i ]; then
		max=`svnlook youngest $SVNROOT/$i`
		if [ $max -gt 0 ]; then
			echo "Already done '$i'."
			continue;
		fi
	fi
	
	# Ignore known ignorables
	if [ "$i" = "Attic" ]; then
		continue
	fi
	if [ "$i" = "archive" ]; then
		continue
	fi

	# Note which one we're on
	echo "Processing $i..."

	# Clear up old repository
	echo "  Clearing last migration..."
	rm -rf $TMPDIR/cvs2svn*

	# Create the subversion module
	echo "  Creating new repository..."
	rm -rf $SVNROOT/$i
	svnadmin create $SVNROOT/$i

	# Create dump of the CVS repository
	echo "  Running cvs2svn..."
	cd /home/users/rossg/cvs2svn
	#./cvs2svn -q \
	./cvs2svn \
		--cvs-revnums \
		--existing-svnrepos \
		--no-default-eol \
		--mime-types=/etc/mime.types \
		--symbol-default=branch \
		--tmpdir=$TMPDIR \
		-s $SVNROOT/$i \
		$CVSROOT/$i \
		>$OUTDIR/$i.out 2>&1

	# Provide a quick summary of the run and cmpress the full log, in 
	# case it needs downloading (big!)
	echo "  Processing output file..."
	tail -100 $OUTDIR/$i.out >$OUTDIR/$i.out.tail
	bzip2 -f $OUTDIR/$i.out &

	# Set up pre-commit hooks
	echo "  Installing pre-commit script."
	(
		cd $SVNROOT/$i/hooks
		head -55 pre-commit.tmpl > pre-commit
		cat <<EOF >> pre-commit
# Run the system-wide pre-commit check
/svn/bin/common-pre-commit \$REPOS \$TXN

EOF
		chmod 755 pre-commit
	)

	# Set up post-commit hooks
	echo "  Installing post-commit script."
	(
		cd $SVNROOT/$i/hooks
		head -48 post-commit.tmpl > post-commit
		cat <<EOF >> post-commit
# Run the system-wide post-commit check
/svn/bin/common-post-commit \$REPOS \$REV

EOF
		chmod 755 post-commit
	)

	# Run dolog_svn to gather bonsai input
	# (TODO: switch to jamesh's version of this script)
	echo "  Gathering bonsai data..."
	x=1
	max=`svnlook youngest $SVNROOT/$i`
	cd /usr/local/mozilla-svn/webtools/bonsai
	while [ $x -lt $[max + 1] ]; do
		./dolog_svn.pl -r $x -p $SVNROOT/$i
		x=$[x + 1]
	done

	# Set perms to allow gnomecvs group-write access
	echo "  Making module read/writable..."
	chown -R gnomecvs:gnomecvs $SVNROOT/$i
	chmod -R g+ws $SVNROOT/$i
}

# Migrate modules in the priority lists first, then any that haven't already
# been done in the remaining list
for i in `cat /home/users/rossg/migration-priority-1.list`; do
	# Special requests for priority treatment
	migrate $i
done
for i in `cat /home/users/rossg/migration-priority-2.list`; do
	# A couple of websites that ought to be swiftly updateable
	migrate $i
done
for i in `cat /home/users/rossg/migration-priority-3.list`; do
	# Automatically determined 'most active' modules
	migrate $i
done
(cd $CVSROOT; ls -t1 >/home/users/rossg/migration-remaining.list)
for i in `cat /home/users/rossg/migration-remaining.list`; do
	# The rest, in alphabetical order
	migrate $i
done

