#!/bin/bash

TMPFILE=/tmp/expand_rcs_tags-$$

cat <<EOF >$TMPFILE 
s;\\\$(.*):\ .*\\\$;\$\\1\$;g
EOF

find $@ -type f | while read i;
do
	sed -r -f $TMPFILE <$i >$i.tmp
	mv $i.tmp $i
done

rm -f $TMPFILE

