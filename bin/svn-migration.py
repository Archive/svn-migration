#!/usr/bin/env python

# This script will hopefully help us manage the migration of GNOME CVS modules
# to Subversion repositories as quickly and efficiently as possible.


MIGRATION_HOME = "/usr/local/svn-migration"
CVS2SVN_HOME = MIGRATION_HOME + "/cvs2svn"
MIMETYPES_FILE = MIGRATION_HOME + "/etc/mime.types"

LIVE_RUN = 0

DO_BONSAI = 1

LOCKDIR = MIGRATION_HOME + "/locks"
TMPDIR = MIGRATION_HOME + "/tmp"
LOGDIR = MIGRATION_HOME + "/logs"
WWWDIR = MIGRATION_HOME + "/www"
TESTDIR = MIGRATION_HOME + "/tests"
CVSROOT = "/cvs/gnome"
SVNROOT = "/svn-test"

CHECK_INTERVAL = 15
LOADAVG_LIMIT = 4

import sys
import os
import dircache
import datetime
import time
import threading
import Queue
import re
from xml.dom.minidom import getDOMImplementation

impl = getDOMImplementation()

MIGRATION_SUCCESS = 1
MIGRATION_FAILURE = 2
MIGRATION_DIFFERENCES = 3

# Migration handlers
#
# There should be one migration handler per CVS module

class MigrationHandler(threading.Thread):
	def __init__(self, manager, module, queue_pos):
		self.manager = manager
		self.module = module
		self.queue_pos = queue_pos
		self.status = ""
		self.start_time = 0
		self.end_time = 0
		self.textdiff_size = 0
		self.result_code = 0

	def init_thread(self):
		threading.Thread.__init__(self, None, None, self.module)

	def set_status(self, status):
		self.status = status
		#sys.stdout.write(self.module + ": " + status + "\n")

	def run(self):
		try:
			self.go()
		except Exception, e:
			self.set_status("%s: %s" % (type(e), e.__str__()))
			self.result_code = MIGRATION_FAILURE

		# Make sure migration marked as finished
		self.end_time = datetime.datetime.now()
		self.manager.callback_done(self)
	
	def go(self):
		# Start the clock
		self.set_status("Starting")
		self.start_time = datetime.datetime.now()
		module = self.module

		# Create a temp dir for cvs2svn.* files
		tmpfiledir = TMPDIR + "/" + module
		if os.access(tmpfiledir, os.F_OK):
			os.system("rm -rf " + tmpfiledir)
		os.mkdir(tmpfiledir)

		# Disable write access to CVS
		self.set_status("Disabling write access to CVS")
		exitcode = 0
		if LIVE_RUN:
			exitcode = os.system("chmod -R ugo-w %s/%s" % (CVSROOT, module))
		if exitcode > 0:
			raise Exception("Unable to disable write access to CVS for %s (chmod exitcode %d)" % (module, exitcode))

		# Create the subversion repository
		self.set_status("Creating fresh subversion repository")
		repodir = SVNROOT + "/" + module
		if os.access(repodir, os.F_OK):
			os.system("rm -rf " + repodir)
		svnadmin_cmd = "svnadmin create " + repodir
		exitcode = os.system(svnadmin_cmd)
		if exitcode > 0:
			raise Exception("Unable to initialise subversion repository for %s (svnadmin exitcode %d)" % (module, exitcode))

		# Some modules have binary files with extensions usually reserved for text files. Handle them.
		normal_repo = 1
		if module == "livecd-project":
			normal_repo = 0

		# Open a logfile to write to
		self.set_status("Opening log file")
		logfile = LOGDIR + "/" + module + ".out"
		cvs2svn_log = open(logfile, "w")

		# Start the main 'cvs2svn' process and monitor it's output
		self.set_status("Starting 'cvs2svn'...")
		os.chdir(CVS2SVN_HOME)
		if normal_repo:
			cvs2svn_cmd = "./cvs2svn --existing-svnrepos --mime-types=%s --eol-from-mime-type --no-default-eol --symbol-default=heuristic --encoding=iso-8859-1 --tmpdir=%s -s %s %s 2>&1" % (MIMETYPES_FILE, tmpfiledir, repodir, CVSROOT + "/" + module)
		else:
			cvs2svn_cmd = "./cvs2svn --existing-svnrepos --symbol-default=heuristic --encoding=iso-8859-1 --tmpdir=%s -s %s %s 2>&1" % (tmpfiledir, repodir, CVSROOT + "/" + module)
		#sys.stderr.write(cvs2svn_cmd + "\n")
		#self.set_status("Running '%s'..." % cvs2svn_cmd)
		cvs2svn_pipe = os.popen(cvs2svn_cmd)
		while(1):
			# Monitor cvs2svn output
			line = cvs2svn_pipe.readline()
			if(line == ""):
				break;

			# Copy to our output buffer regardless
			cvs2svn_log.write(line)
			cvs2svn_log.flush()

			# Parse to update runtime status
			line = line.replace("\n", "")
			if line[0:12] == "----- pass ":
				self.set_status(line[7:])
			if line[0:21] == "Creating Subversion r":
				self.set_status("pass 6: " + line[21:])
			if line[0:21] == "Starting Subversion r":
				self.set_status("pass 9: " + line[21:])

		# Check exit code and report FAILURE
		exitcode = cvs2svn_pipe.close()
		if exitcode > 0:
			raise Exception("cvs2svn returned exit code %d" % exitcode)

		# Perform some checks from the test directory
		self.test(module)
	
		# Set up pre-commit hooks
		self.set_status("Setting pre-commit hook script")
		hookfilename = SVNROOT + "/" + module + "/hooks/pre-commit"
		hookfile = open(hookfilename, "w")
		if not hookfile:
			raise Exception("Could not write pre-commit hook for " + module)
		hookfile.write("""#!/bin/sh

REPOS="$1"
TXN="$2"

# Run the system-wide pre-commit check
/svn/bin/common-pre-commit $REPOS $TXN

""")
		hookfile.close()
		os.system("chmod 755 " + hookfilename)

		# Set up post-commit hooks
		self.set_status("Setting post-commit hook script")
		hookfilename = SVNROOT + "/" + module + "/hooks/post-commit"
		hookfile = open(hookfilename, "w")
		if not hookfile:
			raise Exception("Could not write post-commit hook for " + module)
		hookfile.write("""#!/bin/sh

REPOS="$1"
REV="$2"

# Run the system-wide post-commit check
/svn/bin/common-post-commit $REPOS $REV

""")
		hookfile.close()
		os.system("chmod 755 " + hookfilename)

		# Populate bonsai
		if DO_BONSAI:
			self.set_status("Populating bonsai history database")
			os.system("/var/www/viewcvs-web/svndbadmin rebuild %s" % repodir)

		# Set perms to allow gnomecvs group-write access
		if LIVE_RUN:
			self.set_status("Enabling write access to subversion module...")
			os.system("chown -R gnomecvs:gnomecvs " + repodir)
			os.system("chmod -R ug+ws " + repodir)

		# Provide a quick summary of the run
		self.set_status("Preparing summary file")
		logfile_tail = LOGDIR + "/" + module + ".tail"
		os.system("tail -25 %s > %s" % (logfile, logfile_tail))

		# Compress the full log, in case it needs downloading (big!)
		self.set_status("Compressing cvs2svn output log")
		os.system("bzip2 -f %s" % logfile)

		# All done
		self.set_status("Migration complete")

	def test(self, module):
		# Work in our test directory
		os.chdir(TESTDIR)

		# Check out HEAD from the CVS module 
		self.set_status("Checking out from CVS for comparison...")
		moduletestdir = module + "-HEAD"
		os.system("rm -rf %s %s" % (moduletestdir, module))
		retval = os.system("cvs -q -d %s export -r HEAD %s >/dev/null" %
			(CVSROOT, module))
		if retval > 0:
			raise Exception("Could not check out CVS module for comparison (cvs export returned %d)" % retval)

		# Rename it to '-HEAD'
		os.system("mv %s %s" % (module, moduletestdir))

		# Check out trunk from the subversion repo
		self.set_status("Checking out from Subversion for comparison...")
		repodir = SVNROOT + "/" + module
		repotestdir = module + "-trunk"
		os.system("rm -rf " + repotestdir)
		retval = os.system("svn export -q file://%s/trunk %s" %
			(repodir, repotestdir))
		if retval > 0:
			raise Exception("Could not check out CVS module for comparison (svn export returned %d)" % retval)

		# Remove 'macros' and other subdir if from gnome-common (spoils the diffs)
		in_cvs = os.path.isdir("%s/macros" % moduletestdir)
		in_svn = os.path.isdir("%s/macros" % repotestdir)
		if((in_cvs and not in_svn) and module != "gnome-common"):
			os.system("rm -rf %s/macros" % moduletestdir)
		in_cvs = os.path.isdir("%s/hacking-macros" % moduletestdir)
		in_svn = os.path.isdir("%s/hacking-macros" % repotestdir)
		if((in_cvs and not in_svn) and module != "gnome-common"):
			os.system("rm -rf %s/hacking-macros" % moduletestdir)
		in_cvs = os.path.isdir("%s/intl" % moduletestdir)
		in_svn = os.path.isdir("%s/intl" % repotestdir)
		if((in_cvs and not in_svn) and module != "gnome-common"):
			os.system("rm -rf %s/intl" % moduletestdir)
		in_cvs = os.path.isdir("%s/build" % moduletestdir)
		in_svn = os.path.isdir("%s/build" % repotestdir)
		if((in_cvs and not in_svn)):
			os.system("rm -rf %s/build" % moduletestdir)

		# Convert RCS tags in two checkouts to their unexpanded forms
		retval = os.system(MIGRATION_HOME + "/bin/unexpand-rcs-tags.sh %s %s" %
			(moduletestdir, repotestdir))
		if retval > 0:
			raise Exception("Could not unexpand RCS tags in test check outs")

		# Compare the two checkouts and report differences, remembering
		# whether comparison was clean or not (by looking at filesize of
		# diff file)
		self.set_status("Reporting on differences between checkouts...")
		diff = LOGDIR + "/" + module + "-text.diff"
		os.system("diff -raubN %s %s >%s" %
			(moduletestdir, repotestdir, diff))
		self.textdiff_size = os.stat(diff).st_size;

		# Indicate success/failure
		if self.textdiff_size > 0:
			self.set_status("Completed. Please check for unacceptable differences.");
			self.result_code = MIGRATION_DIFFERENCES
			self.end_time = datetime.datetime.now()
		else:
			self.set_status("Completed with no differences.");
			self.result_code = MIGRATION_SUCCESS
			self.end_time = datetime.datetime.now()

	def add_status_to_node(self, dom, modulenode):
		# Human readable times
		start_time = ""
		if self.start_time.__class__ == datetime.datetime:
			start_time = self.start_time.strftime("%Y-%m-%d %H:%M:%S")
		end_time = ""
		run_time = ""
		if self.end_time.__class__ == datetime.datetime:
			run_time = "%s seconds" % (self.end_time - self.start_time).seconds
			end_time = self.end_time.strftime("%Y-%m-%d %H:%M:%S")

		# Header for this module
		modulenode.setAttribute("id", self.module);
		modulenode.setAttribute("queue", "%d" % self.queue_pos);
		if self.status:
			subnode = modulenode.appendChild(dom.createElement("status"))
			subnode.appendChild(dom.createTextNode(self.status))
		if start_time:
			subnode = modulenode.appendChild(dom.createElement("start_time"))
			subnode.appendChild(dom.createTextNode(start_time))
		if end_time:
			subnode = modulenode.appendChild(dom.createElement("end_time"))
			subnode.appendChild(dom.createTextNode(end_time))
			subnode = modulenode.appendChild(dom.createElement("run_time"))
			subnode.appendChild(dom.createTextNode(run_time))
			subnode = modulenode.appendChild(dom.createElement("result"))
			subnode.setAttribute("textdiff_size", "%d" % self.textdiff_size)
			subnode.setAttribute("code", "%d" % self.result_code)


# The main migration controller
#
# The idea of this is to start with an empty queue. Every few secs, it checks
# to see if any new modules have been added to the queue, and as long as there
# are no more than 2 or 3 other migrations currently running, and load is below
# a safe level, spawn a new migration to handle it. It should also poll any
# existing migrations for a status update, and present a simple results table 
# of any migrations that have already been run, and the status of any
# currently running. It could optionally be extended to send notifications to
# people of when certain modules are ready for use.

class MigrationManager:
	def __init__(self):
		self.migrations = 0
		self.done = 0
		self.start_time = 0
		self.end_time = 0
		self.stayalive = 1
		self.queue = Queue.Queue()
		self.activecount = 0
		self.handlers = []
		self.recent = []
		self.failed = []
		self.alpha = {}

	def run(self):
		# Start the clock
		self.start_time = datetime.datetime.now()

		# For now, just add everything to the queue (testing)
		#modules = dircache.listdir(CVSROOT)
		migration_order = open(MIGRATION_HOME + "/etc/migration-order")
		modules = migration_order.readlines()
		for module in modules:
			modulename = module.strip()
			ignore_modules = ["CVSROOT", "Attic"]
			if modulename in ignore_modules:
				continue
			self.enqueue(module.strip())

		while self.stayalive or self.activecount > 0:
			#sys.stderr.write("=== Check %s ===\n" % datetime.datetime.now().ctime())
			self.check_queue()
			self.report()

			# there were cases of this exception breaking the manager thread
			try:
				time.sleep(CHECK_INTERVAL)
			except Exception, e:
				sys.stderr.write("%s in sleep: %s" % (type(e), e.__str__()))

		self.end_time = datetime.datetime.now()
		self.report()

	def report(self):
		try:
			self.report_status_to_xml()
			self.report_status_to_xml_for_recent()
			self.report_status_to_xml_for_failed()
			self.report_status_to_xml_for_all()
			for key in self.alpha.keys():
				self.report_status_to_xml_for_key(key)
		except Exception, e:
			sys.stderr.write("%s writing status: %s"  % (type(e), e.__str__()))

	def enqueue(self, module):
		self.activecount = self.activecount + 1
		self.migrations = self.migrations + 1

		# Initialiase a migration thread
		migration = MigrationHandler(self, module, self.migrations)

		# Add to the alpha index array
		key = module[0].lower()
		if not key in self.alpha:
			self.alpha[key] = []
		self.alpha[key].append(migration)
		self.handlers.append(migration)
		self.queue.put(migration)

	def check_queue(self):
		# See if resources are available to start another migration
		#sys.stderr.write("Checking queue...\n")
		if self.queue.empty():
			#sys.stderr.write("No requests left in the queue.\n")
			self.stayalive = 0
			return
		loadavg = os.getloadavg()
		if loadavg[0] > LOADAVG_LIMIT:
			#sys.stderr.write("Load average too high at the moment.\n")
			return

		# Run another migration
		migration = self.queue.get()
		migration.init_thread()
		migration.start()
		self.recent.insert(0, migration)
		while(len(self.recent) > 10):
			self.recent.pop()

	def report_status_to_xml(self):
		# Human readable times
		start_time = ""
		if self.start_time.__class__ == datetime.datetime:
			start_time = self.start_time.strftime("%Y-%m-%d %H:%M:%S")
		end_time = ""
		if self.end_time.__class__ == datetime.datetime:
			end_time = self.end_time.strftime("%Y-%m-%d %H:%M:%S")
		last_updated = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

		# Regnerate 'status.xml' file
		doc = impl.createDocument(None, None, None)
		pinode = doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"status.xsl\"")
		doc.appendChild(pinode)
		doc.documentElement = doc.createElement("migration")
		doc.appendChild(doc.documentElement)
		subnode = doc.documentElement.appendChild(doc.createElement("last_updated"))
		subnode.appendChild(doc.createTextNode(last_updated))
		subnode = doc.documentElement.appendChild(doc.createElement("start_time"))
		subnode.appendChild(doc.createTextNode(start_time))
		subnode = doc.documentElement.appendChild(doc.createElement("modules"))
		subnode.appendChild(doc.createTextNode("%d" % self.migrations))
		subnode = doc.documentElement.appendChild(doc.createElement("remaining"))
		subnode.appendChild(doc.createTextNode("%d" % (self.migrations - self.done)))
		subnode = doc.documentElement.appendChild(doc.createElement("done"))
		subnode.appendChild(doc.createTextNode("%d" % self.done))
		if end_time:
			subnode = doc.documentElement.appendChild(doc.createElement("end_time"))
			subnode.appendChild(doc.createTextNode(end_time))
		for key in self.alpha.keys():
			subnode = doc.documentElement.appendChild(doc.createElement("key"))
			subnode.appendChild(doc.createTextNode(key))
			
		statusfile = open("%s/status.xml" % WWWDIR, "w")
		doc.writexml(statusfile)
		statusfile.close()

	def report_status_to_xml_for_all(self):
		# Regenerate alphabetic status files
		doc = impl.createDocument(None, None, None);
		pinode = doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"status.xsl\"")
		doc.appendChild(pinode)
		doc.documentElement = doc.createElement("modules")
		doc.documentElement.setAttribute("key", "all")
		doc.appendChild(doc.documentElement)
		for migration in self.handlers:
			modulenode = doc.documentElement.appendChild(doc.createElement("module"))
			migration.add_status_to_node(doc, modulenode)
		statusfile = open("%s/status-all.xml" % WWWDIR, "w")
		doc.writexml(statusfile)
		statusfile.close()

	def report_status_to_xml_for_recent(self):
		# Regenerate alphabetic status files
		doc = impl.createDocument(None, None, None);
		pinode = doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"status.xsl\"")
		doc.appendChild(pinode)
		doc.documentElement = doc.createElement("modules")
		doc.documentElement.setAttribute("key", "recent")
		doc.appendChild(doc.documentElement)
		for migration in self.recent:
			modulenode = doc.documentElement.appendChild(doc.createElement("module"))
			migration.add_status_to_node(doc, modulenode)
		statusfile = open("%s/status-recent.xml" % WWWDIR, "w")
		doc.writexml(statusfile)
		statusfile.close()

	def report_status_to_xml_for_failed(self):
		# Regenerate failed status file
		doc = impl.createDocument(None, None, None);
		pinode = doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"status.xsl\"")
		doc.appendChild(pinode)
		doc.documentElement = doc.createElement("modules")
		doc.documentElement.setAttribute("key", "failed")
		doc.appendChild(doc.documentElement)
		for migration in self.failed:
			modulenode = doc.documentElement.appendChild(doc.createElement("module"))
			migration.add_status_to_node(doc, modulenode)
		statusfile = open("%s/status-failed.xml" % WWWDIR, "w")
		doc.writexml(statusfile)
		statusfile.close()

	def report_status_to_xml_for_key(self, key):
		# Regenerate alphabetic status files
		doc = impl.createDocument(None, None, None);
		pinode = doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"status.xsl\"")
		doc.appendChild(pinode)
		doc.documentElement = doc.createElement("modules")
		doc.documentElement.setAttribute("key", key)
		doc.appendChild(doc.documentElement)
		for migration in self.alpha[key]:
			modulenode = doc.documentElement.appendChild(doc.createElement("module"))
			migration.add_status_to_node(doc, modulenode)
		statusfile = open("%s/status-%s.xml" % (WWWDIR, key), "w")
		doc.writexml(statusfile)
		statusfile.close()

	def callback_done(self, migration):
		self.activecount = self.activecount - 1
		self.done = self.done + 1
		if migration.result_code == MIGRATION_FAILURE:
			self.failed.append(migration)

# > LOAD
# OK
# > RUN
# RUNNING...
if __name__=="__main__":
	MigrationManager().run()

