#!/usr/bin/env python

import os
import sys
import MySQLdb

CVSBASE = "/cvs/gnome"
MIGRATION_ORDER = "/usr/local/svn-migration/etc/migration-order"
CUTOFF = "2004-01-01"

def get_migration_list():
	db = MySQLdb.connect("localhost", "bonsai", "<password>", "bonsai")
	cursor = db.cursor()
	sql = "select substr(repositories.repository, 6) as repo, count(*) as commits from checkins, repositories where checkins.repositoryid = repositories.id and checkins.ci_when > '%s' group by checkins.repositoryid order by commits desc" % CUTOFF
	cursor.execute(sql)
	repos = []
	while 1:
		data = cursor.fetchone()
		if data == None:
			break
		sys.stdout.write("%s (%d)\n" % (data[0], int(data[1])))
		repos.append(data[0])
	return repos

def get_repo_list():
	modules = os.listdir(CVSBASE)
	filtered = []
	for module in modules:
		ignore_these = ("CVSROOT", "Attic")
		if module in ignore_these:
			continue
		filtered.append(module)
	return filtered

# Get the migration list
migration_list = get_migration_list()

# Add those that exist that haven't had 'recent' commits
others_list = get_repo_list()
for repo in others_list:
	if repo in migration_list:
		continue
	migration_list.append(repo)
	sys.stdout.write("%s (no recent commits)\n" % repo)

# Write out the migration list
migration_order = open(MIGRATION_ORDER, "w+")
for repo in migration_list:
	migration_order.write("%s\n" % repo)
migration_order.close()

# Figure out what's not on the list and dump it



