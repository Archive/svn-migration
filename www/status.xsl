<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" encoding="ISO-8859-1" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />

  <xsl:template match="/">
   <html>
    <head>
     <title>GNOME Subversion Migration: <xsl:value-of select="@title"/></title>

     <link rel="stylesheet" type="text/css" href="/default.css"/>
     <link rel="stylesheet" type="text/css" href="/migration/status.css"/>
     <link rel="icon" type="image/png" href="http://www.gnome.org/img/logo/foot-16.png"/>
     <xsl:if test="/modules/@key='recent' and not(boolean(document('status.xml')/migration/end_time))">
      <meta http-equiv="refresh" content="30"/>
     </xsl:if>
     <meta http-equiv="Content-Type" content="text/html; charset=us-ascii"/>
     <meta name="description" content="GNOME Subversion Migration"/>
    </head>
    <body>

    <div id="hdr">
     <div id="logo">
      <a href="/">
       <img src="http://www.gnome.org/img/spacer" alt="HOME"/>
      </a>
     </div>
     <div id="hdrNav">
      <a href="http://www.gnome.org/">GNOME</a> ::
      <a href="http://developer.gnome.org/">Developers</a> ::
      <a href="http://foundation.gnome.org/">Foundation</a> ::
      <a href="http://www.gnome.org/contact/">Contact</a>
     </div>
    </div>

    <div id="sidebar">
     <p class="section">Status</p>
     <dl>
      <dt>Last updated:</dt>
      <dd><xsl:value-of select="document('status.xml')/migration/last_updated"/></dd>
      <dt>Start time:</dt>
      <dd><xsl:value-of select="document('status.xml')/migration/start_time"/></dd>
      <dt>Modules:</dt>
      <dd><xsl:value-of select="document('status.xml')/migration/modules"/></dd>
      <dt>Remaining:</dt>
      <dd><xsl:value-of select="document('status.xml')/migration/remaining"/></dd>
      <dt>Done:</dt>
      <dd><xsl:value-of select="document('status.xml')/migration/done"/></dd>
      <dt>End time:</dt>
      <dd><xsl:value-of select="document('status.xml')/migration/end_time"/></dd>
     </dl>
    </div>

    <div id="body">
     <h2>GNOME Subversion Migration Status</h2>
     <div id="keys">
      <span><a href="status-all.xml">All</a></span>
      <span><a href="status-recent.xml">Recent</a></span>
      <span><a href="status-failed.xml">Failed</a></span>
      <xsl:for-each select="document('status.xml')/migration/key">
       <span><a href="status-{.}.xml"><xsl:value-of select="."/></a></span>
      </xsl:for-each>
     </div>
     <xsl:apply-templates/>
     <xsl:if test="/modules/@key='recent' and not(boolean(document('status.xml')/migration/end_time))">
      <p>NOTE: This view refreshes automatically every 30 seconds.</p>
     </xsl:if>
    </div>

    <div id="copyright">
     <p>
      Copyright &#169; 2006, <a href="http://www.gnome.org/">The GNOME Project</a><br />
      GNOME and the foot logo are trademarks of the GNOME Foundation.<br />
      <a href="http://validator.w3.org/check/referer">Optimized</a> for <a href="http://www.w3.org/">standards</a>. Hosted by <a href="http://redhat.com/">Red Hat</a>.
      </p>
     </div>
    </body>
   </html>
  </xsl:template>

  <xsl:template match="modules">
   <div class="modules">
    <xsl:apply-templates select="module"/>
   </div>
  </xsl:template>

  <xsl:template match="module">
   <div>
    <xsl:choose>
     <xsl:when test="result/@code=1">
      <xsl:attribute name="class">module success</xsl:attribute>
     </xsl:when>
     <xsl:when test="result/@code=2">
      <xsl:attribute name="class">module failure</xsl:attribute>
     </xsl:when>
     <xsl:when test="result/@code=3">
      <xsl:attribute name="class">module differences</xsl:attribute>
     </xsl:when>
     <xsl:otherwise>
      <xsl:attribute name="class">module</xsl:attribute>
     </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="boolean(end_time)">
     <div class="finished">
      <xsl:if test="result/@textdiff_size &gt; 0">
       <a href="/migration/logs/{@id}-text.diff">HEAD to trunk diff (text)</a>
      </xsl:if>
      <xsl:if test="result/@binarydiff_size &gt; 0">
       <a href="/migration/logs/{@id}-binary.diff">HEAD to trunk diff (binary)</a>
      </xsl:if>
      <a href="/migration/logs/{@id}.out.bz2">cvs2svn output (bzip2'ed)</a>
     </div>
    </xsl:if>
    <span>
     <div class="moduleheader">
      <xsl:value-of select="@id"/>
      <span>(queue position: <xsl:value-of select="@queue"/>)</span>
     </div>
     <div class="moduledetail">
      <span>
       <xsl:if test="boolean(status)">
        <p>Status: <span class="status"><xsl:value-of select="status"/></span></p>
       </xsl:if>
       <xsl:if test="boolean(start_time)">
        <p>Start time: <xsl:value-of select="start_time"/></p>
       </xsl:if>
       <xsl:if test="boolean(end_time)">
        <p>End time: <xsl:value-of select="end_time"/></p>
        <p>Time taken: <xsl:value-of select="run_time"/></p>
       </xsl:if>
      </span>
     </div>
    </span>
   </div>
  </xsl:template>

  <xsl:template match="*"/>

</xsl:stylesheet>
